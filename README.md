# Pong in Pygame

A simple Pong implementation in Pygame.

Designed for https://nick.sarbicki.com/blog/learn-pygame-with-pong/

## Requirements

This has only been tested in Python 3.6.3 using Pygame 1.9.3.

However it should work with all versions of Python (it will work in python 2 
with some minimal changes around the usage of `super` and adding `(object)`
to the class definitions.

## Further info

For further information I'd recommend reading the blog post which goes through
the implementation in more detail and suggests possible expansions.